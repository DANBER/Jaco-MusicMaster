## lookup:genre
genre.txt

## intent:start_song
- Please play some music
- Play me a song
- I want music
- Play (me|) music
- I want to hear music
- Turn on some music
- Start music

## intent:pause_song
- Pause (the|) (music|playback|song)
- Take a break

## intent:unpause_song
- Continue (the|) (music|playback|song)

## intent:next_song
- Skip the song
- Play the next song
- Jump to the next song
- One title further
- (Next|Skip) song

## intent:stop_song
- Stop playing music
- (End|Stop) (the|) (music|playback|song)

## intent:like_song
- I like (this|the) (song|track)
- (this|the) (song|track) sounds good

## intent:dislike_song
- I don't like (this|the) (song|track)
- I hate (this|the) (song|track)

## intent:set_volume
- Set volume levels to value [35](skill_dialogs-numbers2hundred)
- Volume [40](skill_dialogs-numbers2hundred)
- Set volume to [65](skill_dialogs-numbers2hundred)

## intent:set_volume_up
- (Turn up|Increase) the volume
- Turn the music up
- (Play|Make|) (the music|) louder

## intent:set_volume_down
- Turn down (the|) (volume|music)
- (Play|) quieter
- Turn it down
- Decrease the volume
- Play the music quieter

## intent:play_category
- Play song of the category [Classical](genre.txt)
- Category [Nineties](genre.txt)
- Play Category [Jazz](genre.txt)

## intent:play_by_favorite
- Recommend (songs|music) to me
- Complete my favorite (songs|music)
- Play according to my (songs|music) 
- Play recommendations for my (songs|music)

## intent:say_song_name
- What (song|track) is that?
- What's the name of that (song|track)?
- How's this (song|track) called?
