## lookup:genre
genre.txt

## intent:start_song
- Spiele bitte etwas Musik
- Ich möchte Musik
- Spiele Musik ab
- Ich will Musik hören
- Mach Musik an
- (Starte|Spiele|) (mir|) (Musik|ein Song|ein Lied)

## intent:pause_song
- Mache eine Pause
- Pausiere (den Song|das Lied|die Musik|die Wiedergabe)

## intent:unpause_song
- Setze (den Song|die Wiedergabe) fort
- Spiele das Lied weiter
- Spiele wieder weiter

## intent:next_song
- Überspringe das Lied
- Spiele das nächste Lied
- Spiele den nächsten Song
- Springe zum nächsten Lied
- Einen Titel weiter
- Nächster Song
- Überspringe den Song
- Nächstes Lied

## intent:stop_song
- Hör auf Musik zu spielen
- Stoppe die (Musik|Wiedergabe)
- Beende (den Song|das Lied)

## intent:like_song
- Das Lied finde ich gut
- Ich mag den Song
- Das Lied gefällt mir

## intent:dislike_song
- Dieser Song gefällt mir nicht
- Das Lied finde ich schlecht
- Das Lied mag ich nicht

## intent:set_volume
- Setze Lautstärkte auf Wert [35](skill_dialogs-numbers2hundred)
- Lautstärke [40](skill_dialogs-numbers2hundred)
- Stelle Lautstärke auf [65](skill_dialogs-numbers2hundred)

## intent:set_volume_up
- Dreh die Lautstärke rauf
- (Spiele|Mach|) (die Musik|) lauter
- Erhöhe die Lautstärke

## intent:set_volume_down
- Dreh die Lautstärke runter
- (Spiele|Mach|) (die Musik|) leiser
- Verringer die Lautstärke

## intent:play_category
- Spiele Lied der Kategorie [Klassik](genre.txt)
- Kategorie [Neunziger](genre.txt)
- Spiele Kategorie [Jazz](genre.txt)

## intent:play_by_favorite
- Empfehle mir Lieder
- Ergänze meine Lieblingslieder
- Spiele nach meinen Liedern 
- Spiele Empfehlungen zu meinen Liedern

## intent:say_song_name
- Was für ein Lied ist das?
- Wie heißt (das Lied|der Song)?
