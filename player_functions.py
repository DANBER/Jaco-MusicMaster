import json
import os
import random
import re
import subprocess
import threading
import time

import requests
import youtube_functions as ytf

# ==================================================================================================

all_songs: list
disliked_songs = []
stopped_playing = True
current_song = ""
current_youtube_id = ""
playing_from_youtube = False
default_volume = 40
start_volume = 0
play_process = None
file_path = os.path.dirname(os.path.realpath(__file__)) + "/"


# ==================================================================================================


def get_default_volume():
    return default_volume


# ==================================================================================================


def is_song_playing():
    """Checks if there is a process running which plays a song"""

    if play_process is not None and not stopped_playing:
        return True
    else:
        return False


# ==================================================================================================


def get_songs_count():
    if all_songs is not None:
        return len(all_songs)
    else:
        return 0


# ==================================================================================================


def set_default_volume(volume):
    global default_volume

    default_volume = volume


# ==================================================================================================


def get_song_name():
    if current_song != "":
        from_path = not playing_from_youtube
        return format_title(current_song, from_path)
    else:
        return ""


# ==================================================================================================


def format_title(name, from_path=True):
    """Convert title to well formatted text.
    Remove special characters and text in and behind of brackets"""

    if from_path:
        name = os.path.basename(current_song)
        name, _ = os.path.splitext(name)

    name = name.lower()
    name = re.sub("[^A-Za-z0-9ÄÖÜäöüßẞ ()\[\]]+", " ", name)
    name = re.sub("\([^)]*\).*", "", name)
    name = re.sub("\[[^\]]*\].*", "", name)
    name = re.sub("\s+", " ", name)

    return name


# ==================================================================================================


def load_songs(path):
    global all_songs, disliked_songs

    songs = []
    allowed_types = (".mp3", ".wav")

    for path, subdirs, files in os.walk(path):
        for name in files:
            if name.lower().endswith(allowed_types):
                song = os.path.join(path, name)
                songs.append(song)

    path = file_path + "skilldata/disliked_songs.txt"
    if os.path.isfile(path):
        with open(path, "r", encoding="utf-8") as file:
            disliked_songs = file.read().splitlines()

    print("Found {} songs, you disliked {}".format(len(songs), len(disliked_songs)))
    songs = [s for s in songs if s not in disliked_songs]
    random.shuffle(songs)
    all_songs = songs


# ==================================================================================================


def play_in_thread(path):
    global play_process

    if os.path.exists("/tmp/mpvsocket"):
        os.remove("/tmp/mpvsocket")

    if start_volume != 0:
        volume = start_volume
    else:
        volume = default_volume

    cmd = (
        "mpv --no-video --no-audio-display --quiet --input-ipc-server='/tmp/mpvsocket'"
        + " --volume='{}' '{}'"
    )
    cmd = cmd.format(str(volume), path)

    play_process = subprocess.Popen(cmd, shell=True)
    play_process.communicate()
    play_process.wait()
    on_song_ended()


# ==================================================================================================


def play_song(path):
    global stopped_playing

    stopped_playing = False
    print("\nNow playing:", current_song + "\n")

    # Play song in extra non blocking thread
    thread = threading.Thread(target=play_in_thread, args=(path,))
    thread.start()


# ==================================================================================================


def play_song_local():
    """Play random song from filesystem"""
    global all_songs, current_song, current_youtube_id, playing_from_youtube

    # Stop running song if there is one
    stop_song()

    # Choose song to play and append it to the end of the playlist
    path = all_songs.pop(0)
    all_songs.append(path)

    current_youtube_id = ""
    playing_from_youtube = False
    current_song = path
    play_song(path)


# ==================================================================================================


def play_song_youtube(video_id, video_title):
    """Play song with given id from youtube"""
    global current_song, current_youtube_id, playing_from_youtube

    if video_id is None:
        print("No video found, playing song from filesystem instead")
        play_song_local()
        return

    # Stop running song if there is one
    stop_song()

    current_song = video_title
    current_youtube_id = video_id
    playing_from_youtube = True

    # Check if song is  disliked
    if current_song in disliked_songs:
        # Play next song.
        # Based on disliked song to prevent running in dead ends (all next three songs disliked).
        print("Skipping disliked song:", current_song)
        play_next_song()
    else:
        url = ytf.id_to_url(video_id)
        play_song(url)


# ==================================================================================================


def play_by_favorite():
    """If a local song is already playing, get the youtube video id of this song, and set it as
     current_youtube_id, so that when the song ends the next song is based on
     youtube recommendations.
    If no song is playing, get one local song and play it local before playing recommendations."""
    global current_youtube_id, start_volume

    if current_youtube_id != "":
        return "youtube"

    if not is_song_playing():
        # A song is started but snips is saying something afterwards,
        # so the playing volume has to be lowered too
        start_volume = int(get_default_volume() / 2)

        play_song_local()
        ret = "new"
    else:
        ret = "next"

    # Search youtube video id of song
    name = format_title(current_song, from_path=True)
    video_id, video_title = ytf.search(name, randomize=False)
    current_youtube_id = video_id
    return ret


# ==================================================================================================


def play_next_song():
    # Stop running song if there is one
    stop_song()

    if current_youtube_id == "":
        play_song_local()
    else:
        try:
            video_id, video_title = ytf.related_videos(current_youtube_id)
            play_song_youtube(video_id, video_title)
        except requests.exceptions.ConnectionError as e:
            print("YoutubeError:", e)
            play_song_local()
        except requests.exceptions.HTTPError as e:
            print("YoutubeError:", e)
            play_song_local()


# ==================================================================================================


def on_song_ended():
    """Callback after a song ended itself or was stopped playing"""
    global play_process

    play_process = None
    if not stopped_playing:
        # Sleep shortly to give audio player time to release device again
        time.sleep(3)
        play_next_song()


# ==================================================================================================


def pause_song():
    if play_process is None or stopped_playing:
        return

    mcmd = json.dumps({"command": ["set_property", "pause", True]})
    cmd = "echo '{}' | socat - /tmp/mpvsocket".format(mcmd)
    subprocess.Popen(["/bin/bash", "-c", cmd])


# ==================================================================================================


def unpause_song():
    if play_process is None or stopped_playing:
        return

    mcmd = json.dumps({"command": ["set_property", "pause", False]})
    cmd = "echo '{}' | socat - /tmp/mpvsocket".format(mcmd)
    subprocess.Popen(["/bin/bash", "-c", cmd])


# ==================================================================================================


def stop_song():
    global play_process, stopped_playing, current_song

    if play_process is not None and not stopped_playing:
        stopped_playing = True
        current_song = ""

        mcmd = json.dumps({"command": ["quit"]})
        cmd = "echo '{}' | socat - /tmp/mpvsocket".format(mcmd)
        subprocess.Popen(["/bin/bash", "-c", cmd])

        # Wait some time to give mpv time to update
        time.sleep(0.3)


# ==================================================================================================


def set_volume(value, update_default=True):
    global default_volume, start_volume

    # Reset start volume in case it was set
    start_volume = 0

    if play_process is None or stopped_playing:
        return

    if update_default:
        default_volume = value

    mcmd = json.dumps({"command": ["set_property", "volume", value]})
    cmd = "echo '{}' | socat - /tmp/mpvsocket".format(mcmd)
    subprocess.Popen(["/bin/bash", "-c", cmd])


# ==================================================================================================


def dislike_song():
    """Dislike song and remove it from playlist"""
    global all_songs, disliked_songs

    if not is_song_playing():
        return

    with open("skilldata/disliked_songs.txt", "a+", encoding="utf-8") as file:
        file.write(current_song + "\n")

    disliked_songs.append(current_song)
    all_songs = [s for s in all_songs if s != current_song]

    if len(all_songs) > 0:
        play_next_song()
        return "next"
    else:
        return "no songs"


# ==================================================================================================


def like_song(path):
    """Download song if playing from youtube"""

    if not is_song_playing():
        return

    if current_youtube_id == "":
        return "filesystem"
    else:
        url = ytf.id_to_url(current_youtube_id)
        threading.Thread(target=download_and_reload, args=(url, path)).start()
        return "youtube"


# ==================================================================================================


def download_and_reload(url, path):
    ytf.download(url, path)
    load_songs(path)
