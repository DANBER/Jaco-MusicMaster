import os

import player_functions as plf
import requests
import youtube_functions as ytf

from jacolib import assistant

# ==================================================================================================

file_path = os.path.dirname(os.path.realpath(__file__)) + "/"
assist: assistant.Assistant

path_songs = file_path + "skilldata/music/"
reduced_volume_reason = ""


# ==================================================================================================


def callback_start_song(message):
    """Callback to start songs"""

    if plf.get_songs_count() > 0:
        plf.play_song_local()
    else:
        result_sentence = assist.get_random_talk("no_songs")
        assist.publish_answer(result_sentence, message["satellite"])


# ==================================================================================================


def callback_next_song(message):
    """Callback to start songs or play next song"""

    if plf.get_songs_count() > 0:
        plf.play_next_song()
    else:
        result_sentence = assist.get_random_talk("no_songs")
        assist.publish_answer(result_sentence, message["satellite"])


# ==================================================================================================


def callback_stop_song(message):
    """Callback to stop songs"""

    if plf.is_song_playing():
        plf.stop_song()
        result_sentence = assist.get_random_talk("stopped_song")
    else:
        result_sentence = assist.get_random_talk("no_song_playing")

    assist.publish_answer(result_sentence, message["satellite"])


# ==================================================================================================


def callback_say_song_name(message):
    """Callback to say name of song"""

    if plf.is_song_playing():
        result_sentence = plf.get_song_name()
    else:
        result_sentence = assist.get_random_talk("no_song_playing")

    assist.publish_answer(result_sentence, message["satellite"])


# ==================================================================================================


def callback_pause_song(message):
    """Callback to pause song"""

    if plf.is_song_playing():
        plf.pause_song()
    else:
        result_sentence = assist.get_random_talk("no_song_playing")
        assist.publish_answer(result_sentence, message["satellite"])


# ==================================================================================================


def callback_unpause_song(message):
    """Callback to pause song"""

    if plf.is_song_playing():
        plf.unpause_song()
    else:
        result_sentence = assist.get_random_talk("no_song_playing")
        assist.publish_answer(result_sentence, message["satellite"])


# ==================================================================================================


def callback_set_volume(message):
    """Callback to change the volume"""

    if plf.is_song_playing():
        slots = assist.extract_entities(message, "skill_dialogs-numbers2hundred")
        if len(slots) == 1:
            value = int(slots[0])

            if 0 <= value <= 100:
                # Update volume file
                path = file_path + "skilldata/default_volume.txt"
                with open(path, "w+", encoding="utf-8") as file:
                    file.write(str(value))

                plf.set_volume(value, update_default=True)
                return

            else:
                result_sentence = assist.get_random_talk("volume_not_allowed")
        else:
            result_sentence = assist.get_random_talk("volume_not_understood")
    else:
        result_sentence = assist.get_random_talk("no_song_playing")

    assist.publish_answer(result_sentence, message["satellite"])


# ==================================================================================================


def callback_set_volume_up(message):
    """Callback to change the volume"""

    if plf.is_song_playing():
        volume = plf.get_default_volume()
        if volume < 100:
            volume = min(100, volume + 10)
            plf.set_volume(volume, update_default=True)
            return

        else:
            result_sentence = assist.get_random_talk("volume_100")
    else:
        result_sentence = assist.get_random_talk("no_song_playing")

    assist.publish_answer(result_sentence, message["satellite"])


# ==================================================================================================


def callback_set_volume_down(message):
    """Callback to change the volume"""

    if plf.is_song_playing():
        volume = plf.get_default_volume()
        if volume > 1:
            volume = max(1, volume - 10)
            plf.set_volume(volume, update_default=True)
            return

        else:
            result_sentence = assist.get_random_talk("volume_1")
    else:
        result_sentence = assist.get_random_talk("no_song_playing")

    assist.publish_answer(result_sentence, message["satellite"])


# ==================================================================================================


def callback_like_song(message):
    """Callback to like song and download if playing from youtube"""

    if plf.is_song_playing():
        res = plf.like_song(path_songs)

        if res == "youtube":
            result_sentence = assist.get_random_talk("download_song")
        elif res == "filesystem":
            result_sentence = assist.get_random_talk("song_already_saved")
        else:
            result_sentence = assist.get_random_talk("no_song_playing")
    else:
        result_sentence = assist.get_random_talk("no_song_playing")

    assist.publish_answer(result_sentence, message["satellite"])


# ==================================================================================================


def callback_dislike_song(message):
    """Callback to dislike song"""

    if plf.is_song_playing():
        res = plf.dislike_song()

        if res == "next":
            # Successfully changed to next song
            return

        elif res == "no songs":
            result_sentence = assist.get_random_talk("no_songs")
        else:
            result_sentence = assist.get_random_talk("no_song_playing")
    else:
        result_sentence = assist.get_random_talk("no_song_playing")

    assist.publish_answer(result_sentence, message["satellite"])


# ==================================================================================================


def callback_play_category(message):
    """Callback to play a category from youtube"""

    if assist.get_config()["user"]["youtube_api_key"] != "":
        slots = assist.extract_entities(message, "snips_musicmaster_genre")
        if len(slots) == 1:
            genre = slots[0]
            search_key = genre + " " + assist.get_random_talk("genre_search_appendix")

            result_sentence = play_band_category(search_key)
            if result_sentence is None:
                # Found a result which can be played
                return

        else:
            result_sentence = assist.get_random_talk("genre_not_understood")
    else:
        result_sentence = assist.get_random_talk("no_youtube_key")

    assist.publish_answer(result_sentence, message["satellite"])


# ==================================================================================================


def play_band_category(search_key):
    """Play a category or band from youtube"""

    try:
        video_id, video_title = ytf.search(search_key, True)
        plf.play_song_youtube(video_id, video_title)
        return None

    except requests.exceptions.ConnectionError as e:
        print("YoutubeError:", e)
        result_sentence = assist.get_random_talk("no_connection")
    except requests.exceptions.HTTPError as e:
        print("YoutubeError:", e)
        result_sentence = assist.get_random_talk("no_connection")

    return result_sentence


# ==================================================================================================


def callback_play_by_favorite(message):
    """Callback to play song on youtube which are based on a local song"""

    if assist.get_config()["user"]["youtube_api_key"] != "":
        if plf.get_songs_count() > 0:
            ret = plf.play_by_favorite()
            if ret == "new":
                result_sentence = assist.get_random_talk("play_by_favorite_new")
            elif ret == "next":
                result_sentence = assist.get_random_talk("play_by_favorite_next")
            else:
                result_sentence = assist.get_random_talk("play_by_favorite_youtube")
        else:
            result_sentence = assist.get_random_talk("no_songs")
    else:
        result_sentence = assist.get_random_talk("no_youtube_key")

    assist.publish_answer(result_sentence, message["satellite"])


# ==================================================================================================


def callback_auto_volume(message):
    """Lower volume while user or system are speaking and set it back afterwards"""
    global reduced_volume_reason

    if "playing" in message:
        started = message["playing"]
    elif "toggle" in message:
        started = message["toggle"]
    else:
        raise ValueError

    # Make the asr toggle more important than the playing status of asr sounds
    if started:
        if "toggle" in message:
            reduced_volume_reason = "toggle"
    else:
        if reduced_volume_reason == "toggle" and "playing" in message:
            # Ignore sound status message
            return

    if started:
        plf.set_volume(int(plf.get_default_volume() / 2), False)
    else:
        plf.set_volume(plf.get_default_volume(), False)
        reduced_volume_reason = ""


# ==================================================================================================


def main():
    global assist

    assist = assistant.Assistant(repo_path=file_path)

    # Load default volume
    path = file_path + "skilldata/default_volume.txt"
    if os.path.isfile(path):
        with open(path, "r", encoding="utf-8") as file:
            volume = int(file.read())
            plf.set_default_volume(volume)

    # Set youtube api key, this will take some seconds
    api_key = assist.get_config()["user"]["youtube_api_key"]
    if api_key != "":
        ytf.set_yta_key(api_key)

    # Load songs
    plf.load_songs(path_songs)

    # Add callbacks and start
    assist.add_topic_callback("start_song", callback_start_song)
    assist.add_topic_callback("stop_song", callback_stop_song)
    assist.add_topic_callback("next_song", callback_next_song)
    assist.add_topic_callback("pause_song", callback_pause_song)
    assist.add_topic_callback("unpause_song", callback_unpause_song)
    assist.add_topic_callback("set_volume", callback_set_volume)
    assist.add_topic_callback("set_volume_up", callback_set_volume_up)
    assist.add_topic_callback("set_volume_down", callback_set_volume_down)
    assist.add_topic_callback("say_song_name", callback_say_song_name)
    assist.add_topic_callback("like_song", callback_like_song)
    assist.add_topic_callback("dislike_song", callback_dislike_song)
    assist.add_topic_callback("play_category", callback_play_category)
    assist.add_topic_callback("play_by_favorite", callback_play_by_favorite)
    assist.add_topic_callback("Jaco/StreamWav/Status", callback_auto_volume)
    assist.add_topic_callback("Jaco/AsrToggle", callback_auto_volume)
    assist.run()


# ==================================================================================================

if __name__ == "__main__":
    main()
