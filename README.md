# Jaco-MusicMaster
Play your favourite music from local file system or youtube. \
Everything voice controlled. \
Automatically download songs you like by just saying it. 

<br>

What **you can learn** in this skill:

- Play and interact with audio files (with mpv)
- Manage processes in the background

**Complexity**: High

<br>

## Setup

1. Copy your music into the `skilldata/music/` directory \
    (If you know what you're doing, you can also mount it into the container by editing the extra-flags config)

2. Get yourself a free youtube api key, following one of those tutorials. \
This is only needed for youtube functionality, you can use this skill and play local songs without a key.
    * https://developers.google.com/youtube/v3/getting-started
    * https://www.slickremix.com/docs/get-api-key-for-youtube

<br>

## Extras

Download youtube songs manually to your music directory: \
(No youtube key required)
```
# Run in container

python3 Jaco-MusicMaster/download_song.py <youtube-url>
python3 Jaco-MusicMaster/download_song.py "<youtube-url-1>" "<youtube-url-2>" "<...>"
```

Delete songs you did not like from your music directory:
```
# Run in container
python3 Jaco-MusicMaster/delete_disliked_songs.py
```

<br>

## Debugging

Build container image: 

```bash
docker build -t skill_jaco_musicmaster_amd64 - < skills/skills/Jaco-MusicMaster/Containerfile_amd64
```

Run skill: \
(Assumes mqtt-broker already running)

```bash
docker run --network host --ipc host --rm --device /dev/snd \
  --volume `pwd`/skills/skills/Jaco-MusicMaster/:/Jaco-Master/skills/skills/Jaco-MusicMaster/:ro \
  --volume `pwd`/skills/skills/Jaco-MusicMaster/skilldata/:/Jaco-Master/skills/skills/Jaco-MusicMaster/skilldata/ \
  --volume `pwd`/../jacolib/:/Jaco-Master/jacolib/:ro \
  --volume `pwd`/userdata/config/:/Jaco-Master/userdata/config/:ro \
  --volume ~/.asoundrc:/etc/asound.conf:ro \
  -it skill_jaco_musicmaster_amd64 python3 /Jaco-Master/skills/skills/Jaco-MusicMaster/action-music-master.py
```
