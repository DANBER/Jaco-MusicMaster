import os

# ==================================================================================================

file_path = os.path.dirname(os.path.realpath(__file__)) + "/"

# Get disliked songs
ds_path = file_path + "skilldata/disliked_songs.txt"
if os.path.isfile(ds_path):
    with open(ds_path, "r", encoding="utf-8") as file:
        disliked_songs = file.read().splitlines()
else:
    print("You do not have any disliked songs")
    exit()

# Delete songs
c = 0
for d in disliked_songs[:]:
    if os.path.exists(d):
        os.remove(d)
        disliked_songs.remove(d)
        c += 1
print("Deleted {} songs".format(c))
msg = "There are {} songs which could not be found and deleted"
print(msg.format(len(disliked_songs)))

# Create new file and add songs where path was not found (should be youtube urls)
with open(ds_path, "w") as file:
    for d in disliked_songs:
        file.write(d + "\n")
