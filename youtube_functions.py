import random
import subprocess

import requests
from youtube_api import YouTubeDataAPI

# ==================================================================================================

yta = None
yt_api_key = None


# ==================================================================================================


def set_yta_key(api_key):
    global yta, yt_api_key

    yt_api_key = api_key
    try:
        yta = YouTubeDataAPI(api_key)
        print("Youtube api key set")
    except requests.exceptions.ConnectionError as e:
        print("Error setting youtube api key:", e)


# ==================================================================================================


def id_to_url(video_id):
    url = "https://www.youtube.com/watch?v=" + video_id
    return url


# ==================================================================================================


def test_yta():
    """Test if youtube api key is set, there may have been a connection error.
    Try to set it again then"""

    if yta is None and yt_api_key is not None:
        set_yta_key(yt_api_key)

    if yta is None:
        # Something went wrong with setting api key again
        return False
    else:
        return True


# ==================================================================================================


def download(url, path_songs):
    """Download video to given path, optionally wait for output before returning"""

    name = path_songs + "%(title)s.%(ext)s"
    cmd = "youtube-dl --extract-audio --add-metadata --audio-format mp3"
    cmd += " --output '" + name + "' " + url

    p = subprocess.Popen(["/bin/sh", "-c", cmd])
    p.communicate()
    p.wait()
    print("Downloaded song:", url)


# ==================================================================================================


def search(term, randomize=False):
    """Search term at youtube and return one video id and its title"""

    if test_yta() is False:
        return None, None

    max_results = 3 if randomize else 1
    results = yta.search(
        term,
        max_results=max_results,
        order_by="relevance",
        search_type="video",
        topic_id="/m/04rlf",
    )

    if randomize:
        result = random.choice(results)
    else:
        result = results[0]

    video_id = result["video_id"]
    video_title = result["video_title"]
    return video_id, video_title


# ==================================================================================================


def related_videos(video_id):
    """Get a video id and title which is related to given video"""

    if test_yta() is False:
        return None, None

    results = yta.get_recommended_videos(video_id, max_results=3)
    result = random.choice(results)

    video_id = result["video_id"]
    video_title = result["video_title"]
    return video_id, video_title
