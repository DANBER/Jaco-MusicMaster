import argparse
import os

from youtube_functions import download

# ==================================================================================================

file_path = os.path.dirname(os.path.realpath(__file__)) + "/"
path_songs = file_path + "skilldata/music/"

# Read command line input of url
parser = argparse.ArgumentParser(
    description="Download song from youtube to directory in config"
)
parser.add_argument("url", type=str, nargs="+", help="URL of youtube video")
args = parser.parse_args()

# Download songs
msg = "\nSometimes download finished messages are printed too early, "
msg += "but downloading still continues in background\n"
print(msg)
for url in args.url:
    download(url, path_songs)

msg = "\nYou may have to press Enter after all {} songs have been downloaded\n"
print(msg.format(len(args.url)))
